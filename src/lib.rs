pub fn sort_numbers(input: &str) -> Result<Vec<i32>, &'static str> {
    let mut numbers: Vec<i32> = input
        .lines()
        .map(|line| line.trim().parse::<i32>())
        .collect::<Result<Vec<i32>, _>>()
        .map_err(|_| "Failed to parse numbers")?;

    numbers.sort_unstable();
    Ok(numbers)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sort_numbers() {
        let input = "3\n1\n2";
        let expected = vec![1, 2, 3];
        let result = sort_numbers(input).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn test_sort_numbers_with_negative() {
        let input = "0\n-1\n1";
        let expected = vec![-1, 0, 1];
        let result = sort_numbers(input).unwrap();
        assert_eq!(result, expected);
    }

    #[test]
    fn test_sort_numbers_with_invalid_input() {
        let input = "a\n1\n2";
        assert!(sort_numbers(input).is_err());
    }
}
