use std::env;
use std::error::Error;
use std::fs;
use mini_project_8::sort_numbers;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage: {} <input_file>", args[0]);
        eprintln!("input_file: Path to the text file containing numbers");
        std::process::exit(1);
    }

    let input_file = &args[1];

    let contents = fs::read_to_string(input_file)?;

    let sorted_numbers = sort_numbers(&contents)?;

    for number in sorted_numbers {
        println!("{}", number);
    }

    Ok(())
}
