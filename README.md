# mini-project8-yg229

# Rust Command-Line Tool for Sorting Numbers

## Summary
This Rust command-line tool ingests a text file containing numbers, sorts them in ascending order, and outputs the sorted numbers. It showcases fundamental Rust capabilities like file handling, command-line argument parsing, error handling, and algorithm implementation.

## Features
- File Reading: Ingests data from a text file, with each line of the file representing a single number.
- Data Processing: Parses and sorts the numbers from the input file in ascending order.
- Error Handling: Gracefully handles errors, such as invalid input or file not found issues, and provides user-friendly error messages.
- Command-Line Interface: Allows users to specify the input file through the command-line argument.
Unit Testing: Includes tests for key functionalities to ensure reliability.

## Functionalities
### main.rs
main function:
- Purpose: Serves as the entry point for the command-line application.
Functionality: Parses command-line arguments to get the input file name, reads the file, calls sort_numbers to sort the data, and prints the sorted numbers.
### lib.rs
sort_numbers function:
- Purpose: Sorts a list of numbers in ascending order.
Input: A string slice (&str) containing the raw input data.
Output: A Result type, which is Ok(Vec<i32>) containing the sorted numbers on success, or an Err with a static string describing the error on failure.
Functionality: Parses each line of the input string as an integer, collects them into a vector, sorts the vector, and returns the sorted numbers.
### Unit Tests in lib.rs
- test_sort_numbers:

* Purpose: Ensures that sort_numbers correctly sorts a simple list of positive integers.
Functionality: Tests sorting functionality with a basic input.
test_sort_numbers_with_negative:

* Purpose: Checks sort_numbers's ability to handle negative numbers.
Functionality: Ensures that the function correctly sorts a mix of positive and negative integers.
test_sort_numbers_with_invalid_input:

* Purpose: Verifies that sort_numbers properly handles invalid input.
Functionality: Confirms that the function returns an error when non-integer values are included in the input.

Here is the screenshot of unit tests in lib.rs
![](1.png)

## Tests
To run the unit tests, execute the following command:
```cargo test```
Here is the screenshot for the test report
![](2.png)

## Usage
To use the tool, run the following command from the terminal, replacing <test.txt> with the path to your text file containing numbers:
```cargo run --release -- test.txt```
- Here is the screenshot for the input txt file:
![](3.png)
- Here is the test result which sort the input txt in ascending order:
![](4.png)